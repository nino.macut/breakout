#pragma once
#include "Breakout.h"
#include "ECS.h"
#include "Components.h"

class PaddleComponent : public Component {
public:
	PaddleComponent();
	PaddleComponent(int speed);
	~PaddleComponent();

	void Init() override;
	void Update() override;

	TransformComponent* transform = NULL;
	int paddleSpeed = 10;
};