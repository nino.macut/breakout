#pragma once
#include <string>
#include <SDL.h>
#include "ECS.h"
#include "Components.h"

class ColliderComponent : public Component {
public:
	ColliderComponent();
	ColliderComponent(int padX, int padY);
	~ColliderComponent();
	
	void Init() override;
	void Update() override;

	SDL_Rect collider;
	TransformComponent* transform = NULL;
	int paddingX = 0;
	int paddingY = 0;
};