#include "PaddleComponent.h"

PaddleComponent::PaddleComponent() {
	transform = NULL;
}

PaddleComponent::PaddleComponent(int speed) {
	transform = NULL;
	paddleSpeed = speed;
}

PaddleComponent::~PaddleComponent(){}

void PaddleComponent::Init() {
	transform = &entity->GetComponent<TransformComponent>();
}

void PaddleComponent::Update() {
	const Uint8* keystates = SDL_GetKeyboardState(NULL);
	if (keystates[SDL_SCANCODE_LEFT] || keystates[SDL_SCANCODE_A]) {
		if ((transform->position.x - paddleSpeed) >= 0) {
			transform->position.x -= paddleSpeed;
		}
		else {
			transform->position.x = 0;
		}
	}
	if (keystates[SDL_SCANCODE_RIGHT] || keystates[SDL_SCANCODE_D]) {
		if ((transform->position.x + transform->width <= Breakout::w)) {
			transform->position.x += paddleSpeed;
		}
	}
}