#pragma once
#include "ECS.h"
#include "Vector2D.h"

class TransformComponent : public Component {
public:
	TransformComponent();
	TransformComponent(int sc);
	TransformComponent(float x, float y);
	TransformComponent(float x, float y, int h, int w, int sc);
	~TransformComponent();

	void Init() override;

	Vector2D position;
	Vector2D velocity;
	int height = 32;
	int width = 32;
	int scale = 1;
	int speed = 3;
};