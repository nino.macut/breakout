#include "SpriteComponent.h"

SpriteComponent::SpriteComponent() {
	transform = NULL;
	texture = NULL;
	srcRect = { 0, 0, 0, 0 };
	destRect = { 0, 0, 0, 0 };
};

SpriteComponent::SpriteComponent(std::string id) {
	SetTex(id);
}

SpriteComponent::~SpriteComponent(){}

void SpriteComponent::Init() {
	transform = &entity->GetComponent<TransformComponent>();

	srcRect.x = srcRect.y = 0;
	srcRect.w = transform->width;
	srcRect.h = transform->height;
}

void SpriteComponent::SetTex(std::string id)
{
	texture = Breakout::assets->GetTexture(id);
}

void SpriteComponent::Update() {
	destRect.x = static_cast<int>(transform->position.x);
	destRect.y = static_cast<int>(transform->position.y);
	destRect.w = transform->width * transform->scale;
	destRect.h = transform->height * transform->scale;
}

void SpriteComponent::Draw() {
	TextureManager::Draw(texture, srcRect, destRect);
}