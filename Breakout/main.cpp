#include <iostream>
#include "Breakout.h"

using namespace std;

Breakout* game = nullptr;

int main(int argc, char** args) {
	const int FPS = 60;
	const int frameDelay = 1000 / FPS;

	Uint32 frameStart;
	int frameTime;
	bool complete = false;
	int level = 1;
	int ballSpeed = 9;
	
	game = new Breakout();

	game->Init("Breakout", 800, 600, false, ballSpeed);

	while(!complete){
		switch (level) {
		case 1:
			game->LoadLevel("Assets/Levels/level1.xml");
			break;
		case 2:
			game->LoadLevel("Assets/Levels/level2.xml");
			break;
		case 3:
			game->LoadLevel("Assets/Levels/level3.xml");
			break;
		case 4:
			game->isRunning = false;
			complete = true;
			break;
		}

		while (game->Running()) {
			frameStart = SDL_GetTicks();

			game->HandleEvents();
			game->Update();
			game->Render();

			frameTime = SDL_GetTicks() - frameStart;

			if (frameDelay > frameTime) {
				SDL_Delay(frameDelay - frameTime);
			}
		}

		if (game->levelComplete) {
			level++;
		}
		else {

		}
	}

	game->Clean();
	
	return 0;
}