#include "TextureManager.h"

SDL_Texture* TextureManager::LoadTexture(const char* fileName) {
	SDL_Surface* tempSurface = IMG_Load(fileName);
	if (!tempSurface) {
		printf("IMG_Load: %s\n", IMG_GetError());
	}
	SDL_Texture* tex = SDL_CreateTextureFromSurface(Breakout::renderer, tempSurface);
	SDL_FreeSurface(tempSurface);

	return tex;
}

SDL_Texture* TextureManager::CreateTextureFromSurface(SDL_Surface* surface) {
	SDL_Texture* tex = SDL_CreateTextureFromSurface(Breakout::renderer, surface);
	SDL_FreeSurface(surface);

	return tex;
}

void TextureManager::Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest) {
	SDL_RenderCopy(Breakout::renderer, tex, &src, &dest);
}