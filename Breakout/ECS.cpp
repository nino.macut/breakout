#include "ECS.h"

//Entity*******************************************************
void Entity::AddGroup(Group mGroup) {
	groupBitset[mGroup] = true;
	manager.AddToGroup(this, mGroup);
}

void Entity::Update() {
	for (auto& c : components) c->Update();
}

void Entity::Draw() {
	for (auto& c : components) c->Draw();
}

bool Entity::IsActive() const { 
	return active; 
}

void Entity::Destroy() { 
	active = false; 
}

bool Entity::HasGroup(Group mGroup)
{
	return groupBitset[mGroup];
}

void Entity::DelGroup(Group mGroup)
{
	groupBitset[mGroup] = false;
}

//Manager*******************************************************
void Manager::Update() {
	for (auto& e : entities) e->Update();
}

void Manager::Draw() {
	for (auto& e : entities) e->Draw();
}

void Manager::Refresh() {
	for (auto i(0u); i < maxGroups; i++)
	{
		auto& v(groupedEntities[i]);
		v.erase(
			std::remove_if(std::begin(v), std::end(v),
				[i](Entity* mEntity)
				{
					return !mEntity->IsActive() || !mEntity->HasGroup(i);
				}),
			std::end(v));
	}

	entities.erase(std::remove_if(std::begin(entities), std::end(entities),
		[](const std::unique_ptr<Entity>& mEntity)
		{
			return !mEntity->IsActive();
		}),
		std::end(entities));
}

void Manager::AddToGroup(Entity* mEntity, Group mGroup)
{
	groupedEntities[mGroup].emplace_back(mEntity);
}

std::vector<Entity*>& Manager::GetGroup(Group mGroup)
{
	return groupedEntities[mGroup];
}

Entity& Manager::AddEntity() {
	Entity* e = new Entity(*this);
	std::unique_ptr<Entity> uPtr{ e };
	entities.emplace_back(std::move(uPtr));
	return *e;
}