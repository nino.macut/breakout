#include "BrickComponent.h"
#include "Breakout.h"

BrickComponent::BrickComponent(int hp, std::string hitSnd, std::string breakSnd, int breakSc, bool canBrk) {
	healthPoints = hp;
	hitSound = hitSnd;
	breakSound = breakSnd;
	breakScore = breakSc;
	canBreak = canBrk;
}

BrickComponent::~BrickComponent() {};

void BrickComponent::Update() {
		CheckHealth();
}

void BrickComponent::Hit() {
	if (canBreak) {
		healthPoints--;
	}
	PlaySound();
	CheckHealth();
}

void BrickComponent::CheckHealth() {
	if (healthPoints <= 0) {
		Breakout::score += breakScore;
		entity->Destroy();
	}
}

void BrickComponent::PlaySound() {
	if (healthPoints != 0) {
		Breakout::audio->PlaySoundEffect(hitSound);
		return;
	}
	Breakout::audio->PlaySoundEffect(breakSound);
}