#pragma once

#include "SDL.h"
#include "SDL_image.h"
#include <iostream>

class AssetManager;
class SoundManager;

class Breakout
{
public:
	Breakout();
	~Breakout();

	void Init(const char* title, int width, int height, bool fullscreen, int speed);
	void HandleEvents();
	void CheckCollisions();
	void LoadLevel(std::string path);
	void RemoveAllBricks();
	void ResetSettings();
	void UpdateText();
	void Update();
	void Render();
	void Clean();
	bool Running() { return isRunning; }

	static SDL_Renderer* renderer;
	static SDL_Event event;
	static bool levelComplete;
	static bool isRunning;
	static int w;
	static int h;
	static int score;
	static AssetManager* assets;
	static SoundManager* audio;
	static const char* levelName;

	enum class groupLabels : std::size_t
	{
		groupBricks
	};

private:
	int lives = 3;
	struct SDL_Window* window;
};