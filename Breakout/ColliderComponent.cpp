#include "ColliderComponent.h"

ColliderComponent::ColliderComponent(){
	transform = NULL;
	collider.x = 0;
	collider.y = 0;
	collider.w = 0;
	collider.h = 0;
}

ColliderComponent::ColliderComponent(int padX, int padY) {
	transform = NULL;
	collider.x = 0;
	collider.y = 0;
	collider.w = 0;
	collider.h = 0;

	paddingX = padX;
	paddingY = padY;
}

ColliderComponent::~ColliderComponent(){}

void ColliderComponent::Init() {
	transform = &entity->GetComponent<TransformComponent>();
}

void ColliderComponent::Update() {
	collider.x = static_cast<int>(transform->position.x) - paddingX;
	collider.y = static_cast<int>(transform->position.y) - paddingY;
	collider.w = transform->width * transform->scale - 2 * paddingX;
	collider.h = transform->height * transform->scale - 2 * paddingY;
}