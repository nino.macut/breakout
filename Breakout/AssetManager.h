#pragma once

#include <map>
#include <string>
#include "TextureManager.h"
#include "Vector2D.h"
#include "ECS.h"
#include "SDL_ttf.h"
#include "BrickComponent.h"

class AssetManager
{
public:
	AssetManager(Manager* man);
	~AssetManager();

	void CreateBrick(int width, int height, int row, int column, int rowSpacing, int columnSpacing, BrickType brickType);
	void AddTexture(std::string id, const char* path);
	void AddTextureFromSurface(std::string id, SDL_Surface* surface);
	SDL_Texture* GetTexture(std::string id);
	void AddFont(std::string id, std::string path, int fontSize);
	TTF_Font* GetFont(std::string id);

private:
	Manager* manager;
	std::map<std::string, SDL_Texture*> textures;
	std::map<std::string, TTF_Font*> fonts;
};