#include "AssetManager.h"
#include "Components.h"
#include "SoundManager.h"

AssetManager::AssetManager(Manager* man) : manager(man)
{}

AssetManager::~AssetManager()
{
	textures.clear();
}

void AssetManager::CreateBrick(int width, int height, int row, int column, int rowSpacing, int columnSpacing, BrickType brickType)
{
	auto& brick(manager->AddEntity());
	int posx = column * columnSpacing + (column - 1) * width;
	int posy = row * rowSpacing + (row - 1) * height + 50;

	std::string hitAudioName = brickType.Id;
	hitAudioName += "hit";
	Breakout::audio->AddSoundEffect(hitAudioName, brickType.hitSound.c_str());
	std::string breakAudioName = brickType.Id;
	breakAudioName += "break";
	Breakout::audio->AddSoundEffect(breakAudioName, brickType.breakSound.c_str());
	Breakout::assets->AddTexture(brickType.Id, brickType.texture.c_str());

	//std::cout << "Spawned brick (" << row << "," << column << ") at coordinates (" << posx << "," << posy << ")" << std::endl;
	brick.AddComponent<TransformComponent>(posx, posy, height, width, 1);
	brick.AddComponent<SpriteComponent>(brickType.Id);
	brick.AddComponent<BrickComponent>(brickType.hitPoints, hitAudioName, breakAudioName, brickType.breakScore, brickType.canBreak);
	brick.AddComponent<ColliderComponent>(columnSpacing, rowSpacing);

	brick.AddGroup(static_cast<int>(Breakout::groupLabels::groupBricks));
}

void AssetManager::AddTexture(std::string id, const char* path)
{
	textures.emplace(id, TextureManager::LoadTexture(path));
}

void AssetManager::AddTextureFromSurface(std::string id, SDL_Surface* surface) {
	textures.emplace(id, TextureManager::CreateTextureFromSurface(surface));
}

SDL_Texture* AssetManager::GetTexture(std::string id)
{
	return textures[id];
}

void AssetManager::AddFont(std::string id, std::string path, int fontSize)
{
	fonts.emplace(id, TTF_OpenFont(path.c_str(), fontSize));
}

TTF_Font* AssetManager::GetFont(std::string id)
{
	return fonts[id];
}