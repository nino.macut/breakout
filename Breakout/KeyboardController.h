#pragma once
#include "Breakout.h"
#include "ECS.h"
#include "Components.h"

class KeyboardController : public Component {
public:
	TransformComponent* transform;

	void Init() override {
		transform = &entity->getComponent<TransformComponent>();
	}

	void Update() override {
		const Uint8* keystates = SDL_GetKeyboardState(NULL);
		if (keystates[SDL_SCANCODE_LEFT] || keystates[SDL_SCANCODE_A]) transform->position.x -= transform->speed;
		if (keystates[SDL_SCANCODE_RIGHT] || keystates[SDL_SCANCODE_D]) transform->position.x += transform->speed;
	}
};