#pragma once
#include "Components.h"
#include "SDL.h"
#include "TextureManager.h"
#include "AssetManager.h"

class SpriteComponent : public Component {
private:
	TransformComponent* transform;
	SDL_Texture* texture;
	SDL_Rect srcRect, destRect;

public:
	SpriteComponent();
	SpriteComponent(std::string id);
	~SpriteComponent();

	void Init() override;
	void Update() override;
	void Draw() override;

	void SetTex(std::string id);
};