#include "Breakout.h"
#include "TextureManager.h"
#include "ECS.h"
#include "Vector2D.h"
#include "Collision.h"
#include "AssetManager.h"
#include "Components.h"
#include "tinyxml.h"
#include "SoundManager.h"
#include <iterator>
#include <list>

Manager manager;

SDL_Renderer* Breakout::renderer = nullptr;
SDL_Event Breakout::event;

AssetManager* Breakout::assets = new AssetManager(&manager);
SoundManager* Breakout::audio = new SoundManager();

int Breakout::w;
int Breakout::h;
int Breakout::score;
bool Breakout::levelComplete = false;
bool Breakout::isRunning = false;
const char* Breakout::levelName;

SDL_Color White = { 255, 255, 255 };
SDL_Color Black = { 0, 0, 0 };

#define PI 3.14159265358979323846

auto& background(manager.AddEntity());
auto& levelText(manager.AddEntity());
auto& scoreText(manager.AddEntity());
auto& livesText(manager.AddEntity());

auto& paddle(manager.AddEntity());
auto& ball(manager.AddEntity());
auto& bricks(manager.GetGroup(static_cast<int>(Breakout::groupLabels::groupBricks)));

Breakout::Breakout() {
	window = NULL;
}


Breakout::~Breakout() {

}

//Inicijalizacija igre
void Breakout::Init(const char* title, int width, int height, bool fullscreen, int speed) {
	w = width;
	h = height;
	score = 0;

	int flags = 0;
	if (fullscreen) {
		flags = SDL_WINDOW_FULLSCREEN;
	}

	//Ako je rezultat SDL_INIT-a 0, inicijalizacija SDL-a je uspjesna.
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
		std::cout << "SDL initialised!" << std::endl;

		window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, flags);

		if (window) {
			std::cout << "Window created!" << std::endl;
		}

		renderer = SDL_CreateRenderer(window, -1, 0);
		if (renderer) {
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); //Crni prozor.
			std::cout << "Renderer created!" << std::endl;
		}

		isRunning = true;
	}
	else {
		isRunning = false;
	}

	if (TTF_Init() == -1)
	{
		std::cout << "Error : SDL_TTF" << std::endl;
		isRunning = false;
	}

	assets->AddTexture("paddle", "Assets/Images/Paddle.png");
	assets->AddTexture("ball", "Assets/Images/Ball.png");
	assets->AddFont("Alternity", "Assets/Fonts/Alternity.ttf", 10);
	assets->AddFont("APL", "Assets/Fonts/advanced_pixel_lcd.ttf", 10);
	audio->AddSoundEffect("PaddleHit", "Assets/Sounds/Hit1.wav");
	audio->AddSoundEffect("LoseLife", "Assets/Sounds/LoseLife.wav");

	background.AddComponent<TransformComponent>(0, 50, height, width, 1);
	levelText.AddComponent<TransformComponent>(10, 10, 40, 300, 1);
	levelText.AddComponent<SpriteComponent>();
	scoreText.AddComponent<TransformComponent>(400, 10, 20, 400, 1);
	scoreText.AddComponent<SpriteComponent>();
	livesText.AddComponent<TransformComponent>(400, 30, 20, 400, 1);
	livesText.AddComponent<SpriteComponent>();

	paddle.AddComponent<TransformComponent>(((int)(width / 2) - (int)(width / 12)), height - 20, 20, (int)(width / 6), 1);
	paddle.AddComponent<SpriteComponent>("paddle");
	paddle.AddComponent<PaddleComponent>();
	paddle.AddComponent<ColliderComponent>();

	ball.AddComponent<TransformComponent>((int)(width / 2), height - 60, 20, 20, 1);
	ball.AddComponent<SpriteComponent>("ball");
	ball.AddComponent<ColliderComponent>();

	Vector2D* ballVel = &ball.GetComponent<TransformComponent>().velocity;
	int* ballSpeed = &ball.GetComponent<TransformComponent>().speed;
	*ballSpeed = speed;
	ballVel->y = (float)-*ballSpeed;
	ballVel->x = 0;
}

void Breakout::HandleEvents() {
	SDL_PollEvent(&event);

	switch (event.type) {
	case SDL_QUIT:
		isRunning = false;
		break;

	default:
		break;
	}
}

void Breakout::Update() {
	manager.Refresh();
	manager.Update();

	CheckCollisions();

	UpdateText();

	if (lives <= 0) {
		levelComplete = false;
		isRunning = false;

		RemoveAllBricks();
	}
}

void Breakout::Render() {
	SDL_RenderClear(renderer);

	for (auto& b : bricks) {
		b->Draw();
	}

	//Dodati prikaz ostalih tekstova i uljepsati ili mozda napraviti manager za prikaz tekstova.

	//Dodati prikaz levela, scorea i zivota
	

	manager.Draw();

	SDL_RenderPresent(renderer);
}

void Breakout::Clean() {
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	//SDL_Quit();
}

void Breakout::CheckCollisions() {
	SDL_Rect* paddleCol = &paddle.GetComponent<ColliderComponent>().collider;
	SDL_Rect* ballCol = &ball.GetComponent<ColliderComponent>().collider;
	Vector2D* ballPos = &ball.GetComponent<TransformComponent>().position;
	Vector2D* ballVel = &ball.GetComponent<TransformComponent>().velocity;
	int* ballSpeed = &ball.GetComponent<TransformComponent>().speed;

	//Provjera kolizija izmedu lopte i reketa.
	if (Collision::AABB(*paddleCol, *ballCol)) {
		double rel = (paddleCol->x + (paddleCol->w / 2)) - (ballPos->x + (16 / 2));
		double norm = rel / (paddleCol->w / 2);
		double bounce = norm * (5 * PI / 12);
		ballVel->y = static_cast<float>(-*ballSpeed * cos(bounce));
		ballVel->x = static_cast<float>(*ballSpeed * -sin(bounce));
		Breakout::audio->PlaySoundEffect("PaddleHit");
	}

	if (ballPos->y <= 50) ballVel->y = -ballVel->y;
	if (ballPos->y + 16 >= Breakout::h) { 
		ballVel->y = -ballVel->y; 
		lives--; 
		Breakout::audio->PlaySoundEffect("LoseLife");
	}
	if (ballPos->x <= 0 || ballPos->x + 16 >= Breakout::w) ballVel->x = -ballVel->x;

	if (bricks.empty()) {
		levelComplete = true;
		isRunning = false;
		return;
	}

	bool done = true;

	for (auto& b : bricks) {
		if (b->GetComponent<BrickComponent>().canBreak) {
			done = false;
		}
	}

	if (done) {
		levelComplete = true;
		isRunning = false;
		return;
	}

	//Provjera kolizija izmedu lopte i cigli.
	for (auto& b : bricks)
	{
		SDL_Rect* brickCol = &b->GetComponent<ColliderComponent>().collider;
		if (Collision::AABB(*brickCol, *ballCol))
		{
			int xCenterBall = ballCol->x + ballCol->w / 2;
			int yCenterBall = ballCol->y + ballCol->h / 2;

			
			//Check down hit.
			if ((yCenterBall >= brickCol->y + brickCol->h) && (xCenterBall >= brickCol->x) && (xCenterBall <= brickCol->x + brickCol->w)) {
				ballVel->y = -ballVel->y;
			}

			//Check left hit.
			if ((yCenterBall < brickCol->y + brickCol->h) && (yCenterBall > brickCol->y) && (xCenterBall < brickCol->x)) {
				ballVel->x = -ballVel->x;
			}

			//Check right hit.
			if ((yCenterBall < brickCol->y + brickCol->h) && (yCenterBall > brickCol->y) && (xCenterBall > brickCol->x + brickCol->w)) {
				ballVel->x = -ballVel->x;
			}

			//Check up hit.
			if ((yCenterBall <= brickCol->y) && (xCenterBall >= brickCol->x) && (xCenterBall <= brickCol->x + brickCol->w)) {
				ballVel->y = -ballVel->y;
			}

			/* Stari movement lopte.
			if (ballCol->y <= brickCol->y) {
				ballVel->y = -ballVel->y;
			}
			if (ballCol->y >= brickCol->y) {
				ballVel->y = -ballVel->y;
			}*/

			b->GetComponent<BrickComponent>().Hit();
		}
	}

	//Pomicanje lopte.
	ballPos->x += ballVel->x;
	ballPos->y += ballVel->y;
}

void Breakout::LoadLevel(std::string path) {
	//Resetiraj postavke.
	ResetSettings();

	//Otvori datoteku.
	TiXmlDocument doc(path.c_str());
	doc.LoadFile();
	TiXmlHandle docHandle(&doc);

	//Procitaj poredak cigli.
	TiXmlElement* l_bBricks = docHandle.FirstChild("Level").FirstChild("Bricks").ToElement();
	std::string bricksString = l_bBricks->GetText();
	std::string::iterator end_pos = std::remove(bricksString.begin(), bricksString.end(), ' ');
	bricksString.erase(end_pos, bricksString.end());

	//Procitaj strukturu levela.
	TiXmlElement* l_bLevel = docHandle.FirstChild("Level").ToElement();
	int rowCount = atoi(l_bLevel->Attribute("RowCount"));
	int columnCount = atoi(l_bLevel->Attribute("ColumnCount"));
	int rowSpacing = atoi(l_bLevel->Attribute("RowSpacing"));
	int columnSpacing = atoi(l_bLevel->Attribute("ColumnSpacing"));
	const char* backgroundTexture = l_bLevel->Attribute("BackgroundTexture");
	levelName = l_bLevel->Attribute("LevelName");

	//Prikazi naziv levela.
	SDL_Surface* surfaceMessage = TTF_RenderText_Solid(assets->GetFont("APL"), levelName, White);
	std::string levelTxt = levelName;
	levelTxt += "text";
	assets->AddTextureFromSurface(levelTxt, surfaceMessage);
	levelText.GetComponent<TransformComponent>().width = surfaceMessage->w*2;
	levelText.GetComponent<SpriteComponent>().SetTex(levelTxt);
	assets->AddTexture(levelName, backgroundTexture);
	background.AddComponent<SpriteComponent>(levelName);

	//Procitaj tipove cigli.
	std::vector<BrickType> brickTypes;
	TiXmlElement* l_bBrickType = docHandle.FirstChild("Level").FirstChild("BrickTypes").FirstChild("BrickType").ToElement();
	while (l_bBrickType)
	{
		int hitPoints = 0;
		bool canBreak = true;
		std::string hp = l_bBrickType->Attribute("HitPoints");

		if (hp.std::string::compare("Infinite") == 0) {
			hitPoints = 1;
			canBreak = false;
		}
		else {
			hitPoints = atoi(l_bBrickType->Attribute("HitPoints"));
			canBreak = true;
		}
		BrickType brickType = {
			l_bBrickType->Attribute("Id"),
			l_bBrickType->Attribute("Texture"),
			hitPoints,
			l_bBrickType->Attribute("HitSound"),
			l_bBrickType->Attribute("BreakSound"),
			atoi(l_bBrickType->Attribute("BreakScore")),
			canBreak
		};

		brickTypes.push_back(brickType);

		l_bBrickType = l_bBrickType->NextSiblingElement("BrickType");
	}

	//Stvori cigle.
	int brickWidth = (Breakout::w - columnCount * columnSpacing) / columnCount;
	int brickHeight = static_cast<int>(brickWidth / 2);

	int i = 0;
	for (int r = 1; r <= rowCount; r++) {
		for (int c = 1; c <= columnCount; c++) {
			for (unsigned int j = 0; j < brickTypes.size(); j++) {
				std::string c1 = brickTypes.at(j).Id;
				std::string c2(1, bricksString.at(i));

				if (c1.std::string::compare(c2) == 0) {
					assets->CreateBrick(brickWidth, brickHeight, r, c, rowSpacing, columnSpacing, brickTypes.at(j));
				}
			}
			i++;
		}
	}
}

void Breakout::UpdateText() {
	//Update score text.
	std::string sc = "SCORE: ";
	sc += std::to_string(score);
	SDL_Surface* surfaceScore = TTF_RenderText_Solid(assets->GetFont("APL"), sc.c_str(), White);
	scoreText.GetComponent<TransformComponent>().width = surfaceScore->w;
	scoreText.GetComponent<TransformComponent>().height = surfaceScore->h;
	assets->AddTextureFromSurface(sc, surfaceScore);
	scoreText.GetComponent<SpriteComponent>().SetTex(sc);

	//Update lives text.
	std::string lv = "LIVES: ";
	lv += std::to_string(lives);
	SDL_Surface* surfaceLives = TTF_RenderText_Solid(assets->GetFont("APL"), lv.c_str(), White);
	livesText.GetComponent<TransformComponent>().width = surfaceLives->w;
	assets->AddTextureFromSurface(lv, surfaceLives);
	livesText.GetComponent<SpriteComponent>().SetTex(lv);
}

void Breakout::RemoveAllBricks() {
	for (auto& b : bricks)
	{
		b->Destroy();
	}
}

void Breakout::ResetSettings() {
	score = 0;
	lives = 3;
	levelComplete = false;
	isRunning = true;
	Vector2D* ballPos = &ball.GetComponent<TransformComponent>().position;
	ballPos->x = (int)(Breakout::w / 2);
	ballPos->y = Breakout::h - 60;
	Vector2D* ballVel = &ball.GetComponent<TransformComponent>().velocity;
	int* ballSpeed = &ball.GetComponent<TransformComponent>().speed;
	ballVel->y = (float)-*ballSpeed;
	ballVel->x = 0;
}