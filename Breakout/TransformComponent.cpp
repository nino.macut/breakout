#include "TransformComponent.h"

TransformComponent::TransformComponent() {
	position.Zero();
}

TransformComponent::TransformComponent(int sc) {
	position.Zero();
	scale = sc;
}

TransformComponent::TransformComponent(float x, float y) {
	position.x = x;
	position.y = y;
}

TransformComponent::TransformComponent(float x, float y, int h, int w, int sc) {
	position.x = x;
	position.y = y;
	height = h;
	width = w;
	scale = sc;
}

TransformComponent::~TransformComponent() {}

void TransformComponent::Init() {
	velocity.Zero();
}