#pragma once
#include <Map>
#include <SDL_mixer.h>
#include <string>

class SoundManager
{
public:
	SoundManager();
	~SoundManager();

	void AddSoundEffect(std::string id, const char* path);
	void PlaySoundEffect(std::string id);
	Mix_Chunk* GetSoundEffect(std::string id);

private:
	std::map<std::string, Mix_Chunk*> soundEffects;
};