#include "SoundManager.h"
#include <iostream>

SoundManager::SoundManager() {
	int audio_rate = 22050;
	Uint16 audio_format = AUDIO_S16SYS;
	int audio_channels = 2;
	int audio_buffers = 4096;

	if (Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
		std::cout << "Error: " << Mix_GetError() << std::endl;
	}
}
SoundManager::~SoundManager() {

}

void SoundManager::AddSoundEffect(std::string id, const char* path) {
	Mix_Chunk* tmpChunk = Mix_LoadWAV(path);
	if (tmpChunk != nullptr) {
		soundEffects.emplace(id, tmpChunk);
	}
}

void SoundManager::PlaySoundEffect(std::string id) {
	Mix_PlayChannel(-1, GetSoundEffect(id), 0);
}

Mix_Chunk* SoundManager::GetSoundEffect(std::string id)
{
	return soundEffects[id];
}