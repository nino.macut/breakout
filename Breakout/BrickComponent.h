#pragma once
#include "ECS.h"
#include <string>
#include "SoundManager.h"

struct BrickType {
	const char* Id;
	std::string texture;
	int hitPoints;
	std::string hitSound;
	std::string breakSound;
	int breakScore;
	bool canBreak;
};

class BrickComponent : public Component {
public:
	BrickComponent(int hp, std::string hitSnd, std::string breakSnd, int breakSc, bool canBrk);
	~BrickComponent();

	void Update() override;

	void Hit();
	void CheckHealth();
	void PlaySound();

	bool canBreak;
private:
	int healthPoints;
	std::string hitSound;
	std::string breakSound;
	int breakScore;
};